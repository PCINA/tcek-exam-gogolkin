﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tcek_exam.Model
{
    public enum UserRole
    {
        Teacher,
        Secretary
    }
    public class User
    {
        public int UserID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FIO { get; set; }

        public UserRole Role { get; set; }
    }
}
