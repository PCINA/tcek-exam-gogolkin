﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tcek_exam.Model
{
    public class Attestat
    {
        public int AttestatID { get; set; }

        public Student Student { get; set;}

        public int Math { get; set; }

        public int English { get; set; }

        public int Biology { get; set; }

        public int Russian { get; set; }

        public int History { get; set; }

        public int Physics { get; set; }

    }
}
