﻿using System;
using tcek_exam.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace tcek_exam
{
    public class DataContext : DbContext 
    {
        private const string CONNECTION_STRING = @"Data source = localhost\sqlexpress;" +
                                                "Integrated security = true;" +
                                                "Initial catalog = Gogolkin_exam";
        public DbSet<User> Users { get; set; }
        
        public DbSet<Student> Students { get; set; }

        public DbSet<Attestat> Attestats { get; set; }

        public DataContext()
        {
            Database.EnsureCreated();

            InitializeData();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(CONNECTION_STRING);
        }

        private void InitializeData()
        {
            var tUser = Users.FirstOrDefault(x => x.Username == "test_teacher");

            if (tUser == null)
            {
                Users.Add(new User()
                {
                    Username = "test_teacher",
                    Password = "test",
                    Role = UserRole.Teacher
                });

                SaveChanges();
            }

            var sUser = Users.FirstOrDefault(x => x.Username == "test_secretar");

            if (sUser == null)
            {
                Users.Add(new User()
                {
                    Username = "test_secretar",
                    Password = "test",
                    Role = UserRole.Secretary
                });

                SaveChanges();
            }
        }
    }
}
