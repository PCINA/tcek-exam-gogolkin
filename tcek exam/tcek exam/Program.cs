﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using tcek_exam.Forms;

namespace tcek_exam
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            var authForm = new AuthForm();
            authForm.ShowDialog();

            if (AuthForm.SignedUser != null)
            {
                switch (AuthForm.SignedUser.Role)
                {
                    case Model.UserRole.Teacher:
                        var tForm = new TeacherForm();
                        tForm.ShowDialog();
                        break;

                    case Model.UserRole.Secretary:
                        var sForm = new MainForm();
                        sForm.ShowDialog();
                        break;
                }

            }
        }
    }
}
