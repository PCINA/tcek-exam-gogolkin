﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcek_exam.Forms
{
    class DeleteObjectDialog
    {
        public static bool ShowConfirmDialog(string message, Action OnCofmirmAction)
        {
            DialogResult dialogResult = MessageBox.Show(message, "Подтверждение", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                OnCofmirmAction();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
