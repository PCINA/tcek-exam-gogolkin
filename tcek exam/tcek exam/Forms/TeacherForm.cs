﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcek_exam.Forms
{
    public partial class TeacherForm : MaterialSkin.Controls.MaterialForm
    {
        private readonly DataContext context;
        public TeacherForm()
        {
            InitializeComponent();

            context = new DataContext();

            RefreshListView();

            this.listView1.FullRowSelect = true;
            this.listView1.MultiSelect = true;
        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void RefreshListView()
        {
            listView1.Items.Clear();

            foreach (var attestat in context.Attestats
                .Include(x => x.Student))
            {
                string[] items =
                {
                    attestat.AttestatID.ToString(),
                    attestat.Student.FIO,
                    attestat.Math.ToString(),
                    attestat.English.ToString(),
                    attestat.Biology.ToString(),
                    attestat.Russian.ToString(),
                    attestat.History.ToString(),
                    attestat.Physics.ToString(),
                };

                ListViewItem item = new ListViewItem(items);

                listView1.Items.Add(item);
            }

                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void materialRaisedButton4_Click(object sender, EventArgs e)
        {
            Close();
        }

        //add
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            var dialog = new EntityDialog.EntityDialog("Добавить аттестат", "Добавить");

            var studentItem = new EntityDialog.Items.ComboBoxItem()
            {
                Name = "Студент",
                DataSource = new Dictionary<int, string>()
            };

            foreach(var student in context.Students)
            {
                studentItem.DataSource.Add(student.StudentId, student.FIO);
            }

            var math = new EntityDialog.Items.NumericUpDownItem("Math");
            var english = new EntityDialog.Items.NumericUpDownItem("English");
            var biology = new EntityDialog.Items.NumericUpDownItem("Biology");
            var russian = new EntityDialog.Items.NumericUpDownItem("Russian");
            var his  = new EntityDialog.Items.NumericUpDownItem("History");
            var phy = new EntityDialog.Items.NumericUpDownItem("Physics");

            dialog.AddItem(studentItem);
            dialog.AddItem(math);
            dialog.AddItem(english);
            dialog.AddItem(biology);
            dialog.AddItem(russian);
            dialog.AddItem(his);
            dialog.AddItem(phy);

            dialog.OnSuccessAction += () =>
            {
                var entity = new Model.Attestat()
                {
                    Biology = Convert.ToInt32(Convert.ToString(biology.GetValue())),
                    English = Convert.ToInt32(Convert.ToString(english.GetValue())),
                     History = Convert.ToInt32(Convert.ToString(his.GetValue())),
                     Math = Convert.ToInt32(Convert.ToString(math.GetValue())),
                     Physics = Convert.ToInt32(Convert.ToString(phy.GetValue())),
                     Russian = Convert.ToInt32(Convert.ToString(russian.GetValue())),
                     Student = context.Students.Find(studentItem.GetSelectedKey())
                };

                context.Attestats.Add(entity);
                context.SaveChanges();

                RefreshListView();

                dialog.Close();
            };

            dialog.ShowDialog();
        }
      
    }
}
