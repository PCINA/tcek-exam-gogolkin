﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace tcek_exam.Forms
{
    public partial class MainForm : MaterialSkin.Controls.MaterialForm
    {
        private const string STUDENT_PAGE_NAME = "StudentPage";
        private const string USER_PAGE_NAME = "UserPage";

        private readonly DataContext context;
        public MainForm()
        {
            InitializeComponent();
            context = new DataContext();

            InitializeListViews();

            RefreshStudents();
        }

        private void InitializeListViews()
        {
            this.studentListView.FullRowSelect = true;
            this.studentListView.MultiSelect = true;


            this.UserListView.FullRowSelect = true;
            this.UserListView.MultiSelect = true;

            RefreshStudents();
            RefreshUsers();
        }

        private void RefreshStudents()
        {
            this.studentListView.Items.Clear();

            foreach(var student in context.Students)
            {
                string[] items =
                {
                    student.StudentId.ToString(),
                    student.FIO
                };

                var item = new ListViewItem(items);

                studentListView.Items.Add(item);
            }
        }

        private void RefreshUsers()
        {
            this.UserListView.Items.Clear();

            foreach (var user in context.Users)
            {
                string[] items =
                {
                    user.UserID.ToString(),
                    user.Username,
                    user.Password,
                    user.FIO,
                    user.Role == Model.UserRole.Secretary ? "Секретарь" : "Препод"
                };

                var item = new ListViewItem(items);

                UserListView.Items.Add(item);
            }
        }

        private void materialRaisedButton4_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void add_Click(object sender, EventArgs e)
        {
            EntityDialog.EntityDialog dialog = null;

            switch(this.tabControl.SelectedTab.Name)
            {
                case STUDENT_PAGE_NAME:
                    
                    dialog = new EntityDialog.EntityDialog("Добавить стулента", "Добавить");

                    var item = new EntityDialog.Items.TextBoxItem("ФИО");

                    dialog.AddItem(item);

                    dialog.OnSuccessAction += () =>
                    {
                        var student = new Model.Student()
                        {
                            FIO = Convert.ToString(item.GetValue())
                        };

                        context.Students.Add(student);
                        context.SaveChanges();

                        RefreshStudents();

                        dialog.Close();
                    };


                    break;

                case USER_PAGE_NAME:

                    dialog = new EntityDialog.EntityDialog("Добавить студента", "Добавить");

                    var usernameItem = new EntityDialog.Items.TextBoxItem("Логин");
                    var passwordItem = new EntityDialog.Items.TextBoxItem("Пароль");
                    var fioItem = new EntityDialog.Items.TextBoxItem("ФИО");
                    var roleItem = new EntityDialog.Items.ComboBoxItem()
                    {
                        Name = "Роль",
                        DataSource = new Dictionary<int, string>()
                        {
                            {0, "Препод" },
                            {1, "Секретарь" }
                        },
                    };

                    dialog.AddItem(usernameItem);
                    dialog.AddItem(passwordItem);
                    dialog.AddItem(fioItem);
                    dialog.AddItem(roleItem);

                    dialog.OnSuccessAction += () =>
                    {
                        var user = new Model.User()
                        {
                            FIO = Convert.ToString(fioItem.GetValue()),
                            Password = Convert.ToString(passwordItem.GetValue()),
                            Username = Convert.ToString(usernameItem.GetValue()),
                            Role = roleItem.GetSelectedKey() == 0 ? Model.UserRole.Teacher : Model.UserRole.Secretary
                        };

                        context.Users.Add(user);
                        context.SaveChanges();

                        RefreshUsers();

                        dialog.Close();
                    };


                    break;

            }

            dialog.ShowDialog();
            
            
        }

        private void edit_Click(object sender, EventArgs e)
        {

        }

        private void delete_Click(object sender, EventArgs e)
        {

        }

      
        private void materialTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabControl_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            switch (this.tabControl.SelectedTab.Name)
            {
                case STUDENT_PAGE_NAME:

                    RefreshStudents();


                    break;

                case USER_PAGE_NAME:

                    RefreshUsers();

                    break;

            }

        }
    }
}
