﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcek_exam
{
    public partial class AuthForm : MaterialSkin.Controls.MaterialForm
    {
        public static Model.User SignedUser { get; set; }
        public AuthForm()
        {
            InitializeComponent();
        }
        public static Model.User Authorize()
        {
            var form = new AuthForm();

            form.ShowDialog();

            return SignedUser;
        }


        private void SignInButton_Click_1(object sender, EventArgs e)
        {
            using (var context = new DataContext())
            {
                var username = materialSingleLineTextField1.Text;
                var password = materialSingleLineTextField2.Text;

                context.Users
                    .ToList()
                        .ForEach(user =>
                        {
                            if (user.Username == username && user.Password == password)
                            {
                                SignedUser = user;
                                Close();
                            }
                        });

                if (SignedUser == null)
                {
                    MessageBox.Show("Неправильный логин или пароль");
                }
            }
        }
    }
}
